package cz.inventi.tutorial.service.impl;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cz.inventi.tutorial.entity.Role;
import cz.inventi.tutorial.entity.User;
import cz.inventi.tutorial.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final Map<String, User> users;

    public UserServiceImpl() {
        users = new ConcurrentHashMap<>();
        initializeFakeUsers();
    }

    private void initializeFakeUsers() {
        User userWithAdminRole = User.UserBuilder
                .anUser()
                .withEmail("user@admin.cz")
                .withPassword("password")
                .withRoles(Collections.singletonList(Role.ADMIN))
                .build();
        users.put(userWithAdminRole.getEmail(), userWithAdminRole);

        User userWithoutRole = User.UserBuilder
                .anUser()
                .withEmail("user@nothing.cz")
                .withPassword("password")
                .build();
        users.put(userWithoutRole.getEmail(), userWithoutRole);
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = users.get(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("User was not found by username %s", username));
        }

        return user;
    }
}
