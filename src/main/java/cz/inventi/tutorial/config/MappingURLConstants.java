package cz.inventi.tutorial.config;

public final class MappingURLConstants {

    public static final String CARS = "/cars";
    public static final String CAR_BRANDS = "/car_brands";
    public static final String MANAGE = "/manage";
}
