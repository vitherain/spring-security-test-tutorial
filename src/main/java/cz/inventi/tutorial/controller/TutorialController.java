package cz.inventi.tutorial.controller;

import java.util.Arrays;
import java.util.List;

import cz.inventi.tutorial.config.MappingURLConstants;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TutorialController {

    @RequestMapping(value = MappingURLConstants.CAR_BRANDS, method = RequestMethod.GET)
    public List<String> getCarBrands() {
        List<String> brands = Arrays.asList("Ford", "VW", "Škoda");
        return brands;
    }

    @RequestMapping(value = MappingURLConstants.CARS, method = RequestMethod.GET)
    public List<String> getCars() {
        List<String> cars = Arrays.asList("Focus", "Golf", "Octavia");
        return cars;
    }

    @RequestMapping(value = MappingURLConstants.MANAGE, method = RequestMethod.POST)
    public String postManageParams(@RequestBody(required = false) String params) {
        //we can potentially save params here somewhhere
        return params;
    }
}
