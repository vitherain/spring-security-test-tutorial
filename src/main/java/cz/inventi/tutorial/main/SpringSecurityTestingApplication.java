package cz.inventi.tutorial.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "cz.inventi.tutorial")
@EnableAutoConfiguration
public class SpringSecurityTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityTestingApplication.class, args);
	}
}
