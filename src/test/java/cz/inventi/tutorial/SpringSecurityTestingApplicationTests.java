package cz.inventi.tutorial;

import cz.inventi.tutorial.main.SpringSecurityTestingApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringSecurityTestingApplication.class)
public abstract class SpringSecurityTestingApplicationTests {
}
