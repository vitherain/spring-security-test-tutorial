package cz.inventi.tutorial.util;

import java.util.Collections;

import cz.inventi.tutorial.entity.Role;
import cz.inventi.tutorial.entity.User;
import cz.inventi.tutorial.util.matcher.CustomMatchers;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class SecurityVerifier {

    public static void testDenyAdmin(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder
                .with(SecurityMockMvcRequestPostProcessors.user(User.UserBuilder.anUser().withRoles(Collections.singletonList(Role.ADMIN)).build())))

                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    public static void testDenyUserWithoutRole(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder
                .with(SecurityMockMvcRequestPostProcessors.user(new User())))

                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    public static void testUnauthorizedAnonymous(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder)
                //no user here
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    public static void testPermitAdmin(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder
                .with(SecurityMockMvcRequestPostProcessors.user(User.UserBuilder.anUser().withRoles(Collections.singletonList(Role.ADMIN)).build())))

                .andExpect(CustomMatchers.status().isNotNotFound())
                .andExpect(CustomMatchers.status().isNotForbidden());
    }

    public static void testPermitUserWithoutRole(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder
                .with(SecurityMockMvcRequestPostProcessors.user(new User())))

                .andExpect(CustomMatchers.status().isNotNotFound())
                .andExpect(CustomMatchers.status().isNotForbidden());
    }

    public static void testPermitAnonymous(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder) throws Exception {
        mockMvc.perform(requestBuilder)
                //no user here
                .andExpect(CustomMatchers.status().isNotNotFound())
                .andExpect(CustomMatchers.status().isNotForbidden());
    }
}
