package cz.inventi.tutorial.util.matcher;

import org.springframework.test.util.AssertionErrors;
import org.springframework.util.ObjectUtils;

public class EnhancedAssertion {

    public static void assertNotEquals(String message, Object expected, Object actual) {
        if(ObjectUtils.nullSafeEquals(expected, actual)) {
            inverseFail(message, expected, actual);
        }
    }

    public static void assertFalse(String message, boolean condition) {
        if(condition) {
            AssertionErrors.fail(message);
        }
    }

    private static void inverseFail(String message, Object expected, Object actual) {
        throw new AssertionError(message + " expected:<OTHER THAN " + expected + "> but was:<" + actual + ">");
    }
}
