package cz.inventi.tutorial.util.matcher;

import static org.junit.Assert.assertTrue;

import org.springframework.test.web.servlet.ResultMatcher;

public class CustomMatchers {

    public static ResultMatcher redirectedUrlContains(String expectedUrlPart) {
        return (result) -> assertTrue(result.getResponse().getRedirectedUrl().contains(expectedUrlPart));
    }

    public static EnhancedStatusResultMatchers status() {
        return new EnhancedStatusResultMatchers();
    }
}
