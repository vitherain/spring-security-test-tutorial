package cz.inventi.tutorial.util.matcher;

import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.StatusResultMatchers;

public class EnhancedStatusResultMatchers extends StatusResultMatchers {

    private static final String STATUS = "Status";
    private static final String REDIRECT_URL = "RedirectURL";

    public ResultMatcher isNotForbidden() {
        return this.inverseMatcher(HttpStatus.FORBIDDEN);
    }

    public ResultMatcher isNotNotFound() {
        return this.inverseMatcher(HttpStatus.NOT_FOUND);
    }

    private ResultMatcher inverseMatcher(HttpStatus status) {
        return (result) -> assertThatGivenStatusIsNotInResult(result, status);
    }

    private void assertThatGivenStatusIsNotInResult(MvcResult result, HttpStatus status) {
        EnhancedAssertion.assertNotEquals(
                STATUS,
                status.value(),
                result.getResponse().getStatus()
        );
    }
}
