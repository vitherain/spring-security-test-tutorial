package cz.inventi.tutorial.security;

import javax.servlet.Filter;

import cz.inventi.tutorial.SpringSecurityTestingApplicationTests;
import cz.inventi.tutorial.config.MappingURLConstants;
import cz.inventi.tutorial.controller.TutorialController;
import cz.inventi.tutorial.handler.TutorialExceptionHandler;
import cz.inventi.tutorial.util.SecurityVerifier;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class TutorialControllerSecurityTest extends SpringSecurityTestingApplicationTests {

    @Autowired
    private TutorialExceptionHandler tutorialExceptionHandler;

    @Autowired
    private TutorialController tutorialController;

    @Autowired
    private Filter springSecurityFilterChain;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(tutorialController)
                .setControllerAdvice(tutorialExceptionHandler)
                .addFilters(springSecurityFilterChain)
                .build();
    }

    @Test
    public void getCarBrandsPermitAdmin() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(MappingURLConstants.CAR_BRANDS);
        SecurityVerifier.testPermitAdmin(mockMvc, requestBuilder);
    }

    @Test
    public void getCarBrandsPermitUserWithoutRole() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(MappingURLConstants.CAR_BRANDS);
        SecurityVerifier.testPermitUserWithoutRole(mockMvc, requestBuilder);
    }

    @Test
    public void getCarBrandsUnauthorizedAnonymous() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(MappingURLConstants.CAR_BRANDS);
        SecurityVerifier.testUnauthorizedAnonymous(mockMvc, requestBuilder);
    }

    @Test
    public void getCarsPermitAdmin() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(MappingURLConstants.CARS);
        SecurityVerifier.testPermitAdmin(mockMvc, requestBuilder);
    }

    @Test
    public void getCarsPermitUserWithoutRole() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(MappingURLConstants.CARS);
        SecurityVerifier.testPermitUserWithoutRole(mockMvc, requestBuilder);
    }

    @Test
    public void getCarsPermitAnonymous() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(MappingURLConstants.CARS);
        SecurityVerifier.testPermitAnonymous(mockMvc, requestBuilder);
    }

    @Test
    public void postManageParamsPermitAdmin() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(MappingURLConstants.MANAGE);
        SecurityVerifier.testPermitAdmin(mockMvc, requestBuilder);
    }

    @Test
    public void postManageParamsDenyUserWithoutRole() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(MappingURLConstants.MANAGE);
        SecurityVerifier.testDenyUserWithoutRole(mockMvc, requestBuilder);
    }

    @Test
    public void postManageParamsUnauthorizedAnonymous() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(MappingURLConstants.MANAGE);
        SecurityVerifier.testUnauthorizedAnonymous(mockMvc, requestBuilder);
    }
}
