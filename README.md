# Spring Security Test Tutorial #

Tento repozitář je vytvořen pro účely článku na blogu INVENTI.
http://blog.inventi.cz/spring-security/

## Spuštění ##

Naklonujte tento repozitář pomocí *git clone https://vitherain@bitbucket.org/vitherain/spring-security-test-tutorial.git* a poté projekt naimportujte do libovolného IDE (IntelliJ IDEA, STS, ...) jako Maven projekt.

Poté spusťte testy buďto jednotlivě, nebo všechny najednou a experimentujte.